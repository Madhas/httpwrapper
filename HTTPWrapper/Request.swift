//
//  Request.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 08.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public protocol Request {

    associatedtype Serializer: ResponseSerializer
    typealias Response = Serializer.ModelType

    var urlPath: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any]? { get }
    var timeoutInterval: TimeInterval? { get }
    var requestHeaders: [String: String] { get }
    var responseSerializer: Serializer { get }
    var parametersEncoder: ParametersEncoder { get }
}

public extension Request {

    var method: HTTPMethod {
        return .get
    }

    var parameters: [String: Any]? {
        return nil
    }

    var timeoutInterval: TimeInterval? {
        return nil
    }

    var requestHeaders: [String: String] {
        return [:]
    }

    var parametersEncoder: ParametersEncoder {
        return URLParametersEncoder()
    }
}
