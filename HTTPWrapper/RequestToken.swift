//
//  RequestToken.swift
//  HTTPWrapper
//
//  Created by Андрей Овсянников on 19.02.2021.
//  Copyright © 2021 Home. All rights reserved.
//

import Foundation

public class RequestToken {
    
    private let task: URLSessionTask
    
    init(task: URLSessionTask) {
        self.task = task
    }
    
    public func resume() {
        task.resume()
    }
    
    public func suspend() {
        task.suspend()
    }

    public func cancel() {
        task.cancel()
    }
}
