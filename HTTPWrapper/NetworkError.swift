//
//  NetworkError.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 08.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public enum NetworkError: Error {

    case httpError(statusCode: Int)
    case serializationError(reason: String)
    case invalidResponse
}
