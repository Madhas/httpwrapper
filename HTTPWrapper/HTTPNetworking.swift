//
//  HTTPNetworking.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 22.08.2018.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

public protocol HTTPNetworking: class {

    @discardableResult
    func send<T: Request>(request: T, completion: @escaping CompletionHandler<T.Response>) -> RequestToken?
}
