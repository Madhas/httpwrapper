//
//  OptionalProtocol.swift
//  HTTPClient
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol OptionalProtocol {
    var isNone: Bool { get }
    var isSome: Bool { get }
    var value: Any { get }
}

extension Optional: OptionalProtocol {
    var isNone: Bool {
        return !isSome
    }

    var isSome: Bool {
        switch self {
        case .some: return true
        case .none: return false
        }
    }

    var value: Any {
        switch self {
        case .some(let value): return value
        case .none: return self as Any
        }
    }
}
