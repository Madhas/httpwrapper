//
//  HTTPMethod.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 08.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {

    case get = "GET"
    case post = "POST"
}
