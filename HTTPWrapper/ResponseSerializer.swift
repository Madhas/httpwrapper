//
//  ResponseSerializer.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 11.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public protocol ResponseSerializer {

    associatedtype ModelType

    func serialize(data: Data?) -> Result<ModelType>
}

public final class VoidSerializer: ResponseSerializer {

    public init() {}

    public func serialize(data: Data?) -> Result<Void> {
        return .success(value: ())
    }
}

public final class JSONResponseSerializer<ModelType: Decodable>: ResponseSerializer {

    let decoder: JSONDecoder

    public init(decoder: JSONDecoder = JSONDecoder()) {
        self.decoder = decoder
    }

    public func serialize(data: Data?) -> Result<ModelType> {
        let serializationError = NetworkError.serializationError(reason: "Could not decode data from server")
        guard let data = data else {
            return .failure(error: serializationError)
        }

        let model: ModelType
        do {
            model = try decoder.decode(ModelType.self, from: data)
        } catch {
            return .failure(error: serializationError)
        }

        return .success(value: model)
    }
}
